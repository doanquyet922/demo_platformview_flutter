package com.example.flutter_app_demo1
import io.flutter.plugin.common.PluginRegistry.Registrar

object SecondWidgetPlugin {
    fun registerWith(registrar: Registrar) {
        registrar
                .platformViewRegistry()
                .registerViewFactory(
                        "plugins/second_widget", SecondWidgetFactory())
    }
}