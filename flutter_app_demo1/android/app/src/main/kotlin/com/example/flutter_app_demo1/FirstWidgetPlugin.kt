package com.example.flutter_app_demo1
import io.flutter.plugin.common.PluginRegistry.Registrar

object FirstWidgetPlugin {
    fun registerWith(registrar: Registrar) {
        registrar
                .platformViewRegistry()
                .registerViewFactory(
                        "plugins/first_widget", FirstWidgetFactory())
    }
}